﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovementScript : MonoBehaviour
{
    public float movementForce;

    // variable to hold the audio clip that should play when player walks
    public AudioClip footstepSound;

    void Start()
    {

    }
    
    //Called by Unity once per frame
    public void Update()
    {
        // Get our rigid body that we'll need to find the physics information
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        // Find out from the rigidbody what our current horizontal and vertical speeds are
        float currentSpeedH = ourRigidbody.velocity.x;
        float currentSpeedV = ourRigidbody.velocity.y;

        // Get the animator component that we will be using for setting out animation
        Animator ourAnimator = GetComponent<Animator>();

        // Tell our animator what the speeds are
        ourAnimator.SetFloat("SpeedH", currentSpeedH);
        ourAnimator.SetFloat("SpeedV", currentSpeedV);
    }   

    public void MoveUp()
    {
        // Get the RigidBody for movement
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.up * movementForce);


        // Get the audio source so we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
             // Do nothing - our audio source is already playing the sound effect we want
        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }

    }
    public void MoveLeft()
    {
        // Get the RigidBody for movement
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.left * movementForce);

        // Get the audio source so we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want
        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }
    public void MoveDown()
    {
        // Get the RigidBody for movement
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.down * movementForce);

        // Get the audio source so we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want
        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }
    public void MoveRight()
    {
        // Get the RigidBody for movement
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.right * movementForce);

        // Get the audio source so we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want
        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }
}
